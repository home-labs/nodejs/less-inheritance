import { LessInheritance } from "../index.js";


class C1 {

    msg1: string;

    static staticP1 = `class property defined in ${this.name}`;

    private static privateStaticP1: string = `Hello`;

    constructor(msg: string) {
        this.msg1 = `${C1.privateStaticP1} ${msg}!`;
    }

    static staticMethod1() {

    }

    method1() {
        return C1.privateStaticP1;
    }

}

class C2 {

    p2 = 2;

    static staticProp2: string = `class property defined in ${this.name}`;

    static staticMethod2() {
        return C2.staticProp2;
    }

    method2() {
        return C2.staticProp2;
    }

}

class C3 {

    p3 = 3;

    static staticProp3: string = `class property defined in ${this.name}`;

    static staticMethod3() {

    }

    method3() {
        return C3.staticProp3;
    }

}


/*
 * Para se usar um TIPO estático de uma referência de uma classe, usa-se "typeof". Quando não se usa esta declaração, está-se referindo diretamente a referência da classe.
 */
type TStaticReferences = [typeof C1, typeof C2, typeof C3];

type TConstructorsParameters = [
    ConstructorParameters<typeof C1>,
    ConstructorParameters<typeof C2>,
    ConstructorParameters<typeof C3>
];

class ConcreteImplementorExample implements LessInheritance.Mixable<TStaticReferences, TConstructorsParameters> {

    getConstructors(): TStaticReferences {
        return [C1, C2, C3];
    }

    getConstructorsParameters(): TConstructorsParameters {
        return [[`World`], [], []];
    }

}


const mixingContainer = new LessInheritance.MixingContainer(new ConcreteImplementorExample);

const MixedConstructorsExample = mixingContainer.getMixedConstructors();

console.log(MixedConstructorsExample.staticP1)

const mixedConstructor = new MixedConstructorsExample;
console.log(mixedConstructor.msg1)
console.log(mixedConstructor.p2)
