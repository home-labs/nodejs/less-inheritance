import { Union2Intersection } from "./union2Intersection.js";
import { Constructables2Constructors } from "./constructables2Constructors.js";
// import { NewableConstructorFrom } from "./newableConstructorFrom.js";
// import { TypedNewable } from "./typedNewable.js";


export type Newables2Intersection<T extends NewableFunction[]> =
    new (...args: any[]) => Union2Intersection<Constructables2Constructors<T>[number]>;


/*
export type Newables2Intersection2<T extends NewableFunction[]> = {
    [K in keyof T]: {
        new(...args: any[]): Union2Intersection<Constructables2Constructors<T>[number]>
    }
}[number];
/**/

/*
export type Newables2Intersection3<T extends NewableFunction[]> = {
    [K in keyof T]: (arg2CompareSign: NewableConstructorFrom<T[K]>) => void
}[number] extends
    (arg2CompareSign: infer I) => void
        // aparentemente, quando o tipo inferido é um tipo NewableFunction, não é feito o merge do tipo retornado num novo constructível. E quando o operador ternário é usado na function herdada, também não funciona
        ? TypedNewable<I>
        : never
;
/**/

/*
class C1 { }
class C2 { }
class C3 { }

type T1 = Newables2Intersection3<[typeof C1, typeof C2, typeof C3]>;
/**/
