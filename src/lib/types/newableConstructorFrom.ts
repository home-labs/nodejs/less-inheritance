import { Newable } from "./newable.js";

export type NewableConstructorFrom<T> =
    T extends Newable
    ? InstanceType<T> extends Newable
        ? NewableConstructorFrom<InstanceType<T>>
        : InstanceType<T>
    : T
;

/*
class C1 { }
class C2 { }
class C3 { }

type T1 = Constructor<typeof C1>;
type T2 = Constructor<new () => C1>;
type T3 = Constructor<new () => new () => new () => C1>;
type T4 = Constructor<C1>;
/**/
