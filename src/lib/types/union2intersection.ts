export type Union2Intersection<T> =
    (T extends any
        ? (arg2CompareSign: T) => void
        : never
    ) extends (arg2CompareSign: infer I) => void
        ? I
        : never
;

/*
class C1 { }
class C2 { }
class C3 { }

type Type = Union2Intersection<typeof C1 | typeof C2 | typeof C3>;
/**/
