import { NewableConstructorFrom } from "./newableConstructorFrom.js";


export type Constructables2Constructors<T extends NewableFunction[]> =
{
    [K in keyof T]: NewableConstructorFrom<T[K]>
};

/*
class C1 { }
class C2 { }
class C3 { }

type T1 = Constructables2Constructors<[typeof C1, typeof C2, typeof C3]>;
/**/

