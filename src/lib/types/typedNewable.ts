import { Newable } from "./newable.js";

export type TypedNewable<T> =
    T extends Newable
    ? InstanceType<T> extends Newable
        ? TypedNewable<InstanceType<T>>
        : { new(...args: any[]): InstanceType<T> }
    : { new(...args: any[]): T }
;

/*
class C1 { }
class C2 { }
class C3 { }

type T1 = TypedNewable<typeof C1>;
type T2 = TypedNewable<new () => C1>;
type T3 = TypedNewable<new () => new () => new () => C1>;
type T4 = TypedNewable<C1>;
/**/
