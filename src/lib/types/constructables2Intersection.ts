export type Constructables2Intersection<T extends NewableFunction[]> = {
    [K in keyof T]: (arg2CompareSign: T[K]) => void
}[number] extends
    ((arg2CompareSign: infer I) => void)
    ? I
    : never
;

/*
class C1 { }
class C2 { }
class C3 { }

type T1 = ConstructablesIntersection<[typeof C1]>;
type T2 = ConstructablesIntersection<[new () => C1]>;
type T3 = ConstructablesIntersection<[new () => new () => new () => C1]>;
/**/
