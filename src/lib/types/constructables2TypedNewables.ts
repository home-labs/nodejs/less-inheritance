import { TypedNewable } from "./typedNewable.js";


export type Constructables2TypedNewables<T extends NewableFunction[]> =
{
    [K in keyof T]: TypedNewable<T[K]>
};

/*
class C1 { }
class C2 { }
class C3 { }

type T1 = ConstructableList2TypedNewableList<[typeof C1, typeof C2, typeof C3]>;
/**/
