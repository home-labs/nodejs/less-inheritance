import { Newable } from "./types/newable.js";
import { Mixable } from "./mixable.js";
import { Constructables2Intersection } from "./types/constructables2Intersection.js";
import { Newables2Intersection } from "./types/newables2Intersection.js";
import {
    Constructables2TypedNewables
} from "./types/constructables2TypedNewables.js";


// The "Abstraction" of Design Pattern Bridge
export class MixingContainer<
    TTypeofConstructors extends NewableFunction[],
    TConstructorsParameters extends ConstructorParameters<any>[]> {

    private constructorsMap!: Map<TTypeofConstructors[number], TTypeofConstructors[number]>;

    private mixedNewable!: Newable;

    private mixable: Mixable<TTypeofConstructors, TConstructorsParameters>;

    constructor(mixable: Mixable<TTypeofConstructors, TConstructorsParameters>) {

        this.mixable = mixable;

        this.mapConstructors();

        this.mix();
    }

    getMixedConstructors(): Constructables2Intersection<TTypeofConstructors>
        & Newables2Intersection<Constructables2TypedNewables<TTypeofConstructors>> {
        return this.mixedNewable as any;
    }

    private mix(): void {

        let instance: object;

        let newable: Newable;

        const self = this;

        const mixedNewable = class {

            constructor() {

                for (let constructorsMapKey of self.constructorsMap.keys()) {
                    newable = self.constructorsMap
                        .get(constructorsMapKey) as Newable;
                    instance = new newable;
                    self.buildFrom(instance, this);
                }

            }

        };

        for (let staticReference of this.mixable.getConstructors()) {
            self.buildFrom(staticReference, mixedNewable);
        }

        this.mixedNewable = mixedNewable;

    }

    private buildFrom(
        reference: object | TTypeofConstructors[number],
        defineIn: object | TTypeofConstructors[number]): void {

        let propertyDescriptors: PropertyDescriptor | undefined;

        Reflect.ownKeys(reference).forEach(
            (property: string | symbol | number) => {
                propertyDescriptors = Reflect.getOwnPropertyDescriptor(reference, property);

                Reflect.defineProperty(
                    defineIn,
                    property,
                    propertyDescriptors as PropertyDescriptor
                );
            }
        );

    }


    private mapConstructors() {

        let args: any[];

        this.constructorsMap = new Map();

        this.mixable.getConstructors().forEach(
            // Newable is a NewableFunction
            (builder: Function, i: number) => {
                args = this.mixable.getConstructorsParameters()[i];
                this.constructorsMap.set(
                    builder,
                    builder.bind(builder, ...args)
                );
            }
        );

    }

}
