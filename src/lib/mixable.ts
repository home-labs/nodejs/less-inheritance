import { Newable } from "./types/newable.js";


// The "Implementor" of Design Pattern Bridge
export interface Mixable<
    TTypeofConstructors extends NewableFunction[]
    , TConstructorsParameters extends ConstructorParameters<Newable>[]> {

    getConstructors(): TTypeofConstructors;

    getConstructorsParameters(): TConstructorsParameters;

}
